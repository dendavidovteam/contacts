/**
 * @license Gain.Contacts v0.0.0.1
 * (c) 2015 Denis Davidov http://dendavidov.com
 * License: GNU GPL
 */


'use strict';

angular.module('contacts', ['ngRoute','contacts.sevices', 'ngAnimate', 'ui.bootstrap', 'ui.select', 'ngSanitize'])
    .config(['$routeProvider', function($routeProvider) {
        $routeProvider
            .when('/list', {templateUrl: 'views/list.html', controller: ListCtrl})
            .when('/edit/:itemId', {templateUrl: 'views/edit.html', controller: ItemCtrl})
            .otherwise({redirectTo: '/list'});
    }
]);

angular.module('contacts').controller('ModalInstanceCtrl', function ($scope, $uibModalInstance, contactItem) {
    $scope.types = [
        {id: 0, type: 'PHONE', name: 'Телефон'},
        {id: 1, type: 'EMAIL', name: 'E-mail'},
        {id: 2, type: 'MESSENGER', name: 'Messenger'},
        {id: 3, type: 'WEB', name: 'Web-site'}
    ];

    $scope.selectedType = {
        selected : $scope.types[0]
    };

    var findType = function(typeName) {
        for (var i=0;i<$scope.types.length;i++) {
            if ($scope.types[i].type == typeName) {
                return $scope.types[i];
            }
        }
        return $scope.types[0]
    };

    if (contactItem) {
        $scope.selectedType.selected = findType(contactItem.type);
        $scope.contactItem = {
            data: contactItem.data,
            description: contactItem.description,
            type: $scope.selectedType.selected.type
        }
    }

    $scope.ok = function (){
        $scope.contactItem.type = $scope.selectedType.selected.type;
        $uibModalInstance.close($scope.contactItem);
    };

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    };



});