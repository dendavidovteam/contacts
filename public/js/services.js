/* 
 * @license Gain.Contacts v0.0.0.1
 * (c) 2015 Denis Davidov http://dendavidov.com
 * License: GNU GPL
 */


'use strict';

angular.module('contacts.sevices', ['ngResource'])
    .factory('Contacts', function($resource){
        return $resource('contacts');
    }).
    factory('ContactItem', function($http){
        return {
            delete: function(id) {
                return $http.delete('/api/contactItem/'+id)
            },
            update: function(id, data) {
                return $http.put('/api/contactItem/'+id, data)
            },
            create: function(data) {
                return $http.post('/api/contactItem', data)
            }
        }
    }
);