/* 
 * @license Gain.Contacts v0.0.0.1
 * (c) 2015 Denis Davidov http://dendavidov.com
 * License: GNU GPL
 */


function ListCtrl($scope, Contacts){
    $scope.items = Contacts.query({t: 1});

    $scope.openContactEditForm = function(itemId) {
        window.location.hash = "#/edit/" + itemId;
    }
}

function ItemCtrl($scope, $routeParams, Contacts, ContactItem, $uibModal, $log){
    $scope.contacts = Contacts.query({t: 1, id: $routeParams.itemId});

    $scope.deleteContactItem = function(id) {
        ContactItem.delete(id)
            .success(function(){
                $scope.contacts = Contacts.query({t: 1, id: $routeParams.itemId});
            })
    };

    $scope.editContactItem = function () {
        var contactItem = this.contactItem;
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            resolve: {
                contactItem: function() {
                    return contactItem;
                }
            }
        });

        modalInstance.result.then(function (editedContactItem) {
            contactItem.type = editedContactItem.type;
            contactItem.data = editedContactItem.data;
            contactItem.description = editedContactItem.description;

            var data = $.param($scope.contacts);
            ContactItem.update(contactItem._id, contactItem)
                .success(function(data){
                    $scope.contacts = Contacts.query({t: 1, id: $routeParams.itemId});
                })

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };

    $scope.createContactItem = function() {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'myModalContent.html',
            controller: 'ModalInstanceCtrl',
            resolve: {
                contactItem: function() {
                    return null;
                }
            }
        });

        modalInstance.result.then(function (editedContactItem) {
            var contactItem = {
                type: editedContactItem.type,
                data:  editedContactItem.data,
                description : editedContactItem.description,
                contact : $scope.contacts[0]._id
            };
            var data = $.param($scope.contacts);
            ContactItem.create(contactItem)
                .success(function(data){
                    $scope.contacts = Contacts.query({t: 1, id: $routeParams.itemId});
                })

        }, function () {
            $log.info('Modal dismissed at: ' + new Date());
        });
    };



}