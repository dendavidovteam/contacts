var Contact = require('../models/contact').Contact;
var ContactItem = require('../models/contactItem').ContactItem;

exports.get = function(req, res) {
    var task = req.query.t;
    var userId = req.query.id;
    if (task === undefined) {
        res.render('contacts', { title: 'Contacts' });
        return;
    }
    if (userId) {
        Contact.find({_id:userId}, function (err, contacts) {
            ContactItem.find({contact:contacts[0]._id}, function(err, contactItem) {
                contacts[0].contactItems = contactItem;
                res.send(contacts);
            });
        });
    } else {
        Contact.find({}, function (err, userList) {
            res.send(userList);
        });
    }
};