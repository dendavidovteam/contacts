var express = require('express');
var router = express.Router();

var mongoose = require('../libs/mongoose');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Home' });
});

/* GET contacts page. */
router.get('/contacts', function(req, res, next) {
  require('./contacts').get(req, res);
});

/*ContactItem REST API interfaces*/
router.delete('/api/contactItem/:id', function(req, res) {
    var contactItem = mongoose.models.ContactItem;
    contactItem.remove({
        _id: req.params.id
    }, function (err) {
        if (err) {
            res.send(err);
        }
        res.send('OK');
    })
});

router.put('/api/contactItem/:id', function(req, res) {
    var contactItem = mongoose.models.ContactItem;
    contactItem.update({
        _id: req.params.id
    }, req.body, function (err) {
        if (err) {
            res.send(err);
        }
        res.send('OK');
    })
});

router.post('/api/contactItem', function(req, res) {
    var contactItem = new mongoose.models.ContactItem(req.body);
    contactItem.save(function (err) {
        if (err) {
            res.send(err);
        }
        res.send('OK');
    });
});

module.exports = router;
