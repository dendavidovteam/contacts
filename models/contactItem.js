/**
 * Created by denis on 1/11/16.
 */

var mongoose = require('../libs/mongoose'),
    Schema = mongoose.Schema,
    relationship = require('mongoose-relationship');

var contactItemSchema = new Schema({
    type: {
        type: String
    },
    data: {
        type: String
    },
    description: {
        type: String
    },
    contact: {
        type: Schema.ObjectId,
        ref: 'Contact',
        childPath: 'contactItems'
    }
});

contactItemSchema.plugin(relationship, {relationshipPathName: 'contact'});

exports.ContactItem = mongoose.model('ContactItem', contactItemSchema);