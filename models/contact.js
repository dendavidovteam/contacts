var mongoose = require('../libs/mongoose'),
    Schema = mongoose.Schema,
    relationship = require('mongoose-relationship');

var contactSchema = new Schema({
    name: {
        type: String
    },
    birth: {
        type: Date
    },
    contactItems: [{
        type: Schema.ObjectId,
        ref: 'ContactItem'
    }]
});

exports.Contact = mongoose.model('Contact', contactSchema);