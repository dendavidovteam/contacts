var mongoose = require('./libs/mongoose');
var async = require('async');

async.series([
    open,
    dropDatabase,
    requireModels,
    createContacts,
    fillContactItems,
    showResult
],  function(err) {
    mongoose.disconnect();
    process.exit(err ? 255 : 0);
});

function open(callback) {
    mongoose.connection.on('open', callback);
}

function dropDatabase(callback) {
    var db = mongoose.connection.db;
    db.dropDatabase(callback);
}

function requireModels(callback) {
    require('./models/contact');
    require('./models/contactItem');

    async.each(Object.keys(mongoose.models), function(modelName, callback) {
        mongoose.models[modelName].ensureIndexes(callback);
    }, callback)
}



function createContacts(callback) {
    var contacts = [
        {_id:'5692e59774c2176f4b61eb91', name: 'Иванов Иван Антонович', birth: '1983/06/14'},
        {_id:'5692e59774c2176f4b61eb92', name: 'Петров Петр Петрович', birth: '1983/06/15'}
    ];

    async.each(contacts, function(contactData, callback) {
        var contact = new mongoose.models.Contact(contactData);
        contact.save(callback);
    }, callback);
}

function fillContactItems(callback) {
    var contactItems =
        [{type: 'PHONE', data:'+79443674343',contact:'5692e59774c2176f4b61eb91', description:"Рабочий"},
         {type: 'PHONE', data:'+79443434743',contact:'5692e59774c2176f4b61eb91', description:"Домашний"},
         {type: 'PHONE', data:'+79446734343',contact:'5692e59774c2176f4b61eb92', description:"Рабочий"},
         {type: 'PHONE', data:'+79443434343',contact:'5692e59774c2176f4b61eb92', description:"Домашний"},
         {type: 'EMAIL', data:'mail@example.com',contact:'5692e59774c2176f4b61eb92', description:"Личная почта"},
        ]
    ;
    async.each(contactItems, function(contactItemsData, callback) {
        var contactItemsData = new mongoose.models.ContactItem(contactItemsData);
        contactItemsData.save(callback);
    }, callback);
}

function showResult(callback) {
    var Contact = require('./models/contact').Contact;
    Contact.find({}, function(err, contactList){
        console.log(contactList);
    });
    var ContactItem = require('./models/contactItem').ContactItem;
    ContactItem.find({}, function(err, contactItemList){
        console.log(contactItemList);
    });
}